#include <iostream>
#include "assert.h"
#include "Dynamic.cpp"

template <class T>
class DefaultComparator {
public:
    bool operator() (const T& l, const T& r) {
        return l > r;
    }
};

template <class T, class Comparator = std::less<T> >
class Heap {
public:
    Heap() : arr( 0 ), comp( Comparator() ) {}
    Heap(T *arr, size_t size) {
        this->arr = Dynamic<int>(arr, size);
        this->comp = Comparator();
        buildHeap();
    }

    ~Heap() {}

    Heap(const Heap&) = delete;
    Heap(Heap&&) = delete;

    Heap& operator= (const Heap&) = delete;
    Heap& operator= (Heap&&) = delete;

    const T& peekMax() {
        assert( !isEmpty() );
        return arr[0];
    }

    T extractMax() {
        assert( !isEmpty() );
        T result = arr[0];
        arr[0] = arr[arr.size() - 1];
        arr.remove(arr.size() - 1);
        if (!isEmpty()) {
            siftDown(0);
        }
        return result;
    }

    bool isEmpty() const {
        if (arr.size() != 0) {
            return false;
        }
        return true;
    }

    void push(const T& value) {
        arr.append(value);
        siftUp(arr.size() - 1);
    }

    void printHeap() const {
        arr.printArray();
    }
private:
    Dynamic<T> arr;
    Comparator comp;

    void siftUp(size_t index) {
        while (index > 0) {
            int parent = (index - 1) / 2;
            if (!comp(arr[parent], arr[index])) {
                return;
            }
            std::swap(arr[parent], arr[index]);
            index = parent;
        }
    }

    void siftDown(size_t index) {
        size_t left = 2 * index + 1;
        size_t right = 2 * index + 2;
        size_t largest = index;

        if (left < arr.size() && !comp(arr[left], arr[largest])) {
            largest = left;
        }
        if (right < arr.size() && !comp(arr[right], arr[largest])) {
            largest = right;
        }

        if (largest != index) {
            std::swap(arr[index], arr[largest]);
            siftDown(largest);
        }
    }

    void buildHeap() {
        for (int i = arr.size() / 2 - 1; i >= 0; --i) {
            siftDown(i);
        }
    }
};

int greedy(int n, int *arr, int k) {
    Heap<int> pannier(arr, n);
    Dynamic<int> halves;
    int result = 0;
    while (!pannier.isEmpty()) {
        int buf = 0;
        while (!pannier.isEmpty() && buf + pannier.peekMax() <= k) {
            int max = pannier.extractMax();
            buf += max;
            if (max == 1) {
                continue;
            }
            if (!(max == 0)) {
                halves.append(max / 2);
            }
        }
        for (int i = halves.size() - 1; i >= 0; i--) {
            pannier.push(halves[i]);
            halves.remove(i);
        }
        ++result;
    }
    return result;
}

int main() {
    int n = 0;
    int k = 0;
    std::cin >> n;
    int *arr = new int[n];
    for(size_t i = 0; i < n; ++i) {
        std::cin >> arr[i];
    }
    std::cin >> k;
    std::cout << greedy(n, arr, k) << std::endl;
    return 0;
}