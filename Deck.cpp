#include <iostream>
#include <sstream>
#include <assert.h>
#include "Dynamic.cpp"

template <class T>
class Deck {
public:
    Deck() : arr( 0 ), size( 0 ), capacity( 0 ), head( 0 ), tail( 0 ) {}

    ~Deck() {}

    Deck(const Deck&) = delete;
    Deck& operator=(const Deck&) = delete;
    Deck(const Deck&&) = delete;
    Deck& operator=(const Deck&&) = delete;

    void pushFront(const T& value) {
        increaseDeck();
        head = (head - 1 + capacity) % capacity;
        arr[head] = value;
        ++size;
    }

    T popFront() {
        if (isEmpty()) {
            return -1;
        }
        T value = arr[head];
        head = (head + 1) % capacity;
        --size;
        return value;
    }

    void pushBack(const T& value) {
        increaseDeck();
        tail = (tail + 1) % capacity;
        arr[tail] = value;
        ++size;
    }

    T popBack() {
        if (isEmpty()) {
            return -1;
        }
        T value = arr[tail];
        tail = (tail - 1 + capacity) % capacity;
        --size;
        return value;
    }

    bool isEmpty() {
        return size == 0;
    }

private:
    Dynamic<T> arr;
    size_t size;
    size_t capacity;
    size_t head;
    size_t tail;

    void increaseDeck() {
        if (!this->bufIsOverflow()) { return; }

        size_t newCapacity = !capacity ? 1 : capacity * 2;
        Dynamic<int> *buf = new Dynamic<int>;
        *buf = arr;
        for(size_t i = 0; i < size; i++) {
            arr[i] = (*buf)[head];
            head = (head + 1) % capacity;
        }
        head = 0;
        tail = !capacity ? 0 : capacity - 1;
        capacity = newCapacity;
        delete buf;    
    }

    bool bufIsOverflow() {
        return size == capacity;
    }
};

void run(std::istream& input, std::ostream& output) {
    Deck<int> d;
    int n = 0;
    bool result = true;
    input >> n;
    for (int i = 0; i < n && result; ++i) {
        int command = 0;
        int value = 0;
        input >> command >> value;
        switch (command) {
            case 1:
                d.pushFront(value);
                break;
            case 2:
                if (d.isEmpty()) {
                    result = result && value == -1;
                } else {
                    result = value == d.popFront();
                }
                break;
            case 3:
                d.pushBack(value);
                break;
            case 4:
                if (d.isEmpty()) {
                    result = result && value == -1;
                } else {
                    result = result && value == d.popBack();
                }
                break;
            default:
                assert(false);
        }
    }
    output << (result ? "YES" : "NO");
}

void testDeck() {
    { // 1й тест из условия
        std::stringstream input;
        std::stringstream output;
        input << "3 1 44 3 50 2 44";
        run( input, output );
        assert( output.str() == "YES" );
    }

    { // 2й тест из условия
        std::stringstream input;
        std::stringstream output;
        input << "2 2 -1 1 10";
        run( input, output );
        assert( output.str() == "YES" );
    }

    { // 3й тест из условия
        std::stringstream input;
        std::stringstream output;
        input << "2 3 44 4 66";
        run( input, output );
        assert( output.str() == "NO" );
    }
}

int main() {
//    run(std::cin, std::cout);
    testDeck();
    return 0;
}