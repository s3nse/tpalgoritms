#include <iostream>
#include "Dynamic.cpp"

int county(int n, int k) {
    int arr[n];
    for (size_t i = 0; i < n; ++i) {
        arr[i] = static_cast<int>(i) + 1;
    }

    if (k == 1) {
        return arr[n - 1];
    }

    int isDivided = 0;
    int size = n;
    while (size > 0) {
        int deleted = 0;
        for (size_t j = 0; j < size - deleted; ++j) {
            ++isDivided;
            if (isDivided == k) {
                ++deleted;
                if (j + deleted != size) {
                    isDivided = 1;
                } else {
                    isDivided = 0;
                    break;
                }
            }
            arr[j] = arr[j + deleted];
        }
        size -= deleted;
    }
    return arr[0];
}

int countyWithDynamic(int n, int k) {
    Dynamic<int> arr = Dynamic<int>(n);
    for (int i = 0; i < n; ++i) {
        arr.append(i + 1);
    }

    if (k == 1) {
        return arr[n - 1];
    }

    int isDivided = 0;
    while (arr.count() != 1) {
        for(int i = 0; i < arr.count(); ++i) {
            ++isDivided;
            if (isDivided == k) {
                arr.remove(i);
                isDivided = 0;
                --i;
            }
        }
    }
    let value = arr[0];
    arr.~Dynamic();
    return value;
}

int main() {
    int N = 0;
    int k = 0;
    std::cin >> N >> k;
    std::cout << county(N, k) << std::endl;
    return 0;
}