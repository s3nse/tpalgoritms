#include <iostream>

template <class T>
class Dynamic {
public:
    Dynamic() : arr( nullptr ), _capacity( 0 ), _size( 0 ) {}

    Dynamic(int count) {
        arr = new T[count];
        for (size_t i = 0; i < count; ++i) {
            arr[i] = 0;
        }
        _capacity = count;
        _size = count;
    }

    Dynamic(T *arr, size_t size) {
        _size = size;
        this->arr = new T[size];
        std::copy(arr, arr + _size, this->arr);
    }

    ~Dynamic() {
        delete[] arr;
    }

    Dynamic(const Dynamic& other) {
        this->_size = other._size;
        this->_capacity = other._capacity;
        this->arr = new T[_size];
        std::copy(other.arr, other.arr + _size, this->arr);
    }

    Dynamic &operator= (const Dynamic &other) {
        if (this != &other) {
            this->_size = other._size;
            this->_capacity = other._capacity;
            this->arr = new T[_size];
            std::copy(other.arr, other.arr + _size, this->arr);
        }
        return *this;
    }

    size_t size() const {
        return _size;
    }

    size_t capacity() const {
        return _capacity;
    }

    void append(T value) {
        increase();
        arr[_size] = value;
        ++_size;
    }

    void remove(int index) {
        std::copy(arr + index + 1, arr + _size, arr + index);
        _size--;
    }

    T &operator[](int index) {
        increase();
        if (index >= _size) {
            size_t buf = _size;
            _size = index + 1;
            increase();
            for (size_t i = buf; i < _size; ++i) {
                arr[i] = 0;
            }
        }
        return arr[index];
    }

    void printArray() const {
        for (size_t i = 0; i < _size; ++i) {
            std:: cout << "[" << arr[i] << "]";
        }
        std::cout << std::endl;
    }

private:
    T *arr;
    size_t _capacity;
    size_t _size;

    void increase() {
        if (_size + 1 < _capacity) { return; }

        int max = std::max(_size, _capacity);
        int new_capacity = max * 2;
        T *newArr = new T[new_capacity];
        std::copy(arr, arr + max, newArr);
        delete[] arr;
        arr = newArr;
        _capacity = new_capacity;
    }
};