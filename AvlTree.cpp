#include <iostream>
#include <stack>
#include <sstream>

template <class T>
struct DefaultComparator {
    int operator() (const T& l, const T& r) const {
        if (l < r) return -1;
        if (l > r) return 1;
        return 0;
    }
};

template <class Key, class Value, class Comparator = DefaultComparator<Key> >
class AvlTree {
	struct Node {
		Key key;
		Value value;
		Node* left;
		Node* right;
		int height;
		int childCount;

		Node(const Key& key, const Value& value) : 
			key(key),
			value(value),
			left(nullptr),
			right(nullptr),
			height(1),
			childCount(0) {}
	};
public:

	AvlTree(Comparator comp = Comparator()) : root(nullptr), nodesCount(0), comp(comp) {}

	~AvlTree() {
		freeTree(root);
	}

	void freeTree(Node* node) {
		if (!node) {
			return;
		}

		freeTree(node->left);
		freeTree(node->right);
		delete node;
	}

	Value* find(const Key& key) {
		return findAux(root, key);
	}

	void insert(const Key& key, const Value& value) {
		root = insertAux(root, key, value);
	}

	void erase(const Key& key) {
		root = eraseAux(root, key);
	}

	void insertAndSetPos(const Key& key, size_t& pos) {
		root = insertAndSetPosAux(root, key, pos);
	}
	
	Key findKey(size_t& pos) {
		return findKeyAux(root, pos);
	}

private:
	Node* root;
	size_t nodesCount;
	Comparator comp;

	Value* findAux(Node* node, const Key& key) {
		if (!node) {
			return nullptr;
		}

		int comparision = comp(node->key, key);
		if (comparision == -1) {
			findAux(node->right, key);
		} else {
			findAux(node->left, key);
		}
		return &node->value;
	}

	Node* insertAux(Node* node, const Key& key, const Value& value) {
		if (!node) {
			nodesCount++;
			return new Node(key, value);
		}

		int comparision = comp(node->key, key);
		if (comparision == -1) {
			node->right = insertAux(node->right, key, value);
		} else {
			node->left = insertAux(node->left, key, value);
		}

		return balance(node);
	}

	Node* eraseAux(Node* node, const Key& key) {
		if (!node) {
			return nullptr;
		}
		
		int comparision = comp(node->key, key);

		if (comparision == -1) {
			node->right = eraseAux(node->right, key);
		} else if (comparision == 1) {
			node->left = eraseAux(node->left, key);
		} else {
			Node* left = node->left;
			Node* right = node->right;

			Node* minNode = node;

			delete node;

			if (!right) {
				return left;
			}

			minNode->right = popMin(right, minNode);
			minNode->left = left;

			return balance(minNode);
		}
		return balance(node);
	}

	Node* insertAndSetPosAux(Node* node, const Key& key, size_t& pos) {
		if (!node) {
			nodesCount++;
			return new Node(key, pos);
		}

		int comparision = comp(node->key, key);
		if (comparision == -1) {
			node->right = insertAndSetPosAux(node->right, key, pos);
		} else if (comparision == 1) {
			node->right ? pos += getChildCount(node->right) + 2 : ++pos;
			node->left = insertAndSetPosAux(node->left, key, pos);
		}

		return balance(node);
	}

	Key findKeyAux(Node* node, size_t& pos) {
		size_t curPos = node->right ? getChildCount(node->right) + 1 : 0;

		if (curPos == pos) {
			return node->key;
		} else if (curPos < pos) {
			pos -= curPos;
			pos--;
			return findKeyAux(node->left, pos);
		} else {
			return findKeyAux(node->right, pos);
		}
	}

	Node* popMin(Node* node, Node* result) {
		if (!node->left) {
			result = new Node(node->key, node->value);
			return node->right;
		}

		node->left = popMin(node->left, result);
		return balance(node);
	}

	Node* balance(Node* node) {
		fixHeight(node);
		fixChildCount(node);

		int bFactor = balanceFactor(node);
		if (bFactor == 2) {
			if(balanceFactor(node->right) < 1) {
				node->right = rotateRight(node->right);
			}
			return rotateLeft(node);
		} else if (bFactor == -2) {
			if(balanceFactor(node->left) > -1) {
				node->left = rotateLeft(node->left);
			}
			return rotateRight(node);
		}
		
		return node;
	}

	void fixHeight(Node* node) {
		node->height = std::max(getHeight(node->left), getHeight(node->right)) + 1;
	}

	int getHeight(Node* node) {
		return node ? node->height : 0;
	}

	void fixChildCount(Node* node) {
		int childCount = 0;
		
		if (node->right) {
			childCount++;
		}
		if (node->left) {
			childCount++;
		}
		childCount += getChildCount(node->right) + getChildCount(node->left);
		node->childCount = childCount;
	}

	int getChildCount(Node* node) {
		return node ? node->childCount : 0;
	}

	int balanceFactor(Node* node) {
		return getHeight(node->right) - getHeight(node->left);
	}

	Node* rotateLeft(Node* node) {
		Node* right = node->right;

		node->right = right->left;

		right->left = node;

		fixHeight(node);
		fixHeight(right);
		fixChildCount(node);
		fixChildCount(right);

		return right;
	}

	Node* rotateRight(Node* node) {
		Node* left = node->left;

		node->left = left->right;

		left->right = node;

		fixHeight(node);
		fixHeight(left);
		fixChildCount(node);
		fixChildCount(left);

		return left;
	}
};

void run(std::istream& input, std::ostream& output) {
	size_t commandCount = 0;
	input >> commandCount;

	AvlTree<int, int> tree;

	for (size_t i = 0; i < commandCount; i++) {
		int command = 0;
		input >> command;
		switch (command) {

		case 1: {
			size_t soldierHeight = 0;
			input >> soldierHeight;

			size_t soldierPosition = 0;
			tree.insertAndSetPos(soldierHeight, soldierPosition);
			output << soldierPosition << std::endl;
			break;
		}

		case 2: {
			size_t soldierPosition = 0;
			input >> soldierPosition;

			size_t key = tree.findKey(soldierPosition);
			tree.erase(key);
			break;
		}

		default:
			break;
		}
	}
}

int main() {
	run(std::cin, std::cout);
	return 0;
}