#include <iostream>
#include <stack>
#include <sstream>

template <class Key, class Value, class Comparator = std::less<Key> >
class SearchTree {
    struct Node {
        Key key;
        Value value;
        Node* left;
        Node* right;

        Node(const Key& key, const Value& value) : key(key), value(value), left(nullptr), right(nullptr) {}
    };
public:
    SearchTree(Comparator comp = Comparator()) : root(nullptr), nodesCount(0), comp(comp) {}

    ~SearchTree() {
        if (!root) {
            return;
        }

        std::stack<Node*> stack;
        stack.push(root);

        Node* curNode = nullptr;

        while (!stack.empty()) {
            curNode = stack.top();
            stack.pop();

            if (curNode->left) {
                stack.push(curNode->left);
            }
            if (curNode->right) {
                stack.push(curNode->right);
            }

            delete curNode;
        }
    }

    void insert(const Key& key, const Value& value) {
        if (!root) {
            root = new Node(key, value);
            nodesCount++;
            return;
        }
        
        Node* curNode = root;
        Node* prevNode = nullptr;

        while (curNode) {
            prevNode = curNode;
            if (comp(curNode->key, key)) {
                curNode = curNode->right;
            } else {
                curNode = curNode->left;
            }
        }

        if (comp(prevNode->key, key)) {
            prevNode->right = new Node(key, value);
        } else {
            prevNode->left = new Node(key, value);
        }
        nodesCount++;
        return;
    }

    void postOrder(void (*func)(Key)) {
        if (!root) {
            return;
        }

        std::stack<Node*> stack, stackForWork;
        stack.push(root);

        Node* curNode = nullptr;

        while (!stack.empty()) {
            curNode = stack.top();
            stack.pop();

            stackForWork.push(curNode);

            if (curNode->left) {
                stack.push(curNode->left);
            }
            if (curNode->right) {
                stack.push(curNode->right);
            }
        }
        
        while (!stackForWork.empty()) { 
            Node* node = stackForWork.top();
            stackForWork.pop();
            func(node->key);
        }
    }
    
private:
    Node* root;
    size_t nodesCount;
    Comparator comp;
};

void run(std::istream& input) {
    size_t N = 0;
    long long value = 0;
    input >> N;
    SearchTree<long long, long long> tree;
    for(size_t i = 0; i < N; i++) {
        input >> value;
        tree.insert(value, value);
    }

    auto printKey = [](long long key) {
        std::cout << key << " ";
    };

    tree.postOrder(printKey);
    std::cout << std::endl;
}

int main() {
    run(std::cin);
    return 0;
}