#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include "assert.h"

template<class T> struct HashFunc;

template<> struct HashFunc<std::string> {
    size_t operator() (const std::string& key, const size_t a) {
        size_t hash = 0;
        for (auto i = key.begin(); i != key.end(); i++) {
            hash = (hash * a + *i);
        }
        return hash;
    }
};

template <class Hash = HashFunc<std::string> >
class HashTable {
    struct Cell {
    private:
        const std::string empty = "o4j2vdf71l";
        const std::string deleted = "zg10kl34o3";

    public:
        Cell() : key(empty), value(empty) {}

        Cell(const std::string& key, const std::string& value): key(key), value(value) {}

        std::string key;
        std::string value;

        Cell &operator= (const Cell &other) {
            this->key = other.key;
            this->value = other.value;
            return *this;
        }

        void setDeleted() {
            key = deleted;
        }

        bool isDeleted() {
            return key == deleted;
        }

        bool isEmpty() {
            return key == empty;
        }        
    };

public:
    HashTable(Hash hash = Hash()) : bucketsCount(8), itemsCount(0), hash(hash) {
        bucket = new std::vector<Cell>();
        bucket->resize(bucketsCount);
    }

    ~HashTable() {
        delete bucket;
    }

    bool insert(const std::string& key, const std::string& value) {
        if (4 * itemsCount > 3 * bucketsCount) {
            grow();
        }

        size_t indexOfDeletedCell = -1;

        for (size_t i = 0; i < bucketsCount; i++) {
            size_t hashIndex = mainHash(key, i, bucketsCount);

            if (bucket->at(hashIndex).isEmpty()) {
                Cell cell = {key, value};
                bucket->at(hashIndex) = cell;
                itemsCount++;
                return true;
            }

            if (bucket->at(hashIndex).key == key) {
                return false;
            }

            if (bucket->at(hashIndex).isDeleted() && indexOfDeletedCell == -1) {
                indexOfDeletedCell = hashIndex;
            }
        }

        if (indexOfDeletedCell != -1) {
            bucket->at(indexOfDeletedCell).key = key;
            bucket->at(indexOfDeletedCell).value = value;
            itemsCount++;
            return true;
        }

        return false;
    }

    std::string* find(const std::string& key) {
        if (itemsCount == 0) {
            return nullptr;
        }

        for (size_t i = 0; i < bucketsCount; i++) {
            size_t hashIndex = mainHash(key, i, bucketsCount);
            if (bucket->at(hashIndex).key == key) {
                return &bucket->at(hashIndex).value;
            }

            if (bucket->at(hashIndex).isEmpty()) {
                return nullptr;
            }
        }
        return nullptr;
    }

    bool erase(const std::string& key) {
        for (size_t i = 0; i < bucketsCount; i++) {
            size_t hashIndex = mainHash(key, i, bucketsCount);
            if (bucket->at(hashIndex).key == key) {
                bucket->at(hashIndex).setDeleted();
                itemsCount--;
                return true;
            }

            if (bucket->at(hashIndex).isEmpty()) {
                return false;
            }
        }

        return false;
    }

    void printHash() {
        for (size_t i = 0; i < bucketsCount; i++) {
            if (!bucket->at(i).isEmpty() && !bucket->at(i).isDeleted()) {
                std::cout << "[" << bucket->at(i).key << "]";
            } else {
                std::cout << "[00]";
            }
        }
        std::cout << std::endl;
    }

private:
    std::vector<Cell>* bucket;
    size_t bucketsCount;
    size_t itemsCount;
    Hash hash;

    void grow() {
        std::vector<Cell> oldBucket = *bucket;
        size_t oldBucketsCount = bucketsCount;

        bucket = new std::vector<Cell>();
        bucketsCount *= 2;
        itemsCount = 0;
        bucket->resize(bucketsCount);

        for (size_t i = 0; i < oldBucketsCount; i++) {
            if (oldBucket[i].isDeleted() && oldBucket[i].isEmpty()) {
                continue;
            }

            for (size_t j = 0; j < bucketsCount; j++) {
                size_t hashIndex = mainHash(oldBucket[i].key, j, bucketsCount);

                if (bucket->at(hashIndex).isEmpty()) {
                    bucket->at(hashIndex) = oldBucket[i];
                    itemsCount++;
                    break;
                }
            }
        }
    }

    size_t mainHash(const std::string& key, size_t i, size_t hashSize) {
        size_t h1 = hash(key, 7);
        size_t h2 = hash(key, 13);
        h2 = (h2 / 2) * 2 + 1;
        return (h1 + i * h2) % hashSize;
    }
};

void run(std::istream& input, std::ostream& output) {
    char operation = '\0';
    std::string key = "";
    
    HashTable<> hash;
    while (input >> operation >> key) {
        bool res = false;
        switch (operation) {
            case '+':
                res = hash.insert(key, key);
                break;
            case '-':
                res = hash.erase(key);
                break;
            case '?':
                res = (hash.find(key) != nullptr);
                break;
            default:
                break;
        }
        output << (res ? "OK" : "FAIL") << std::endl;
    }
}

void testHashTable() {
    {
        std::stringstream input;
        std::stringstream output;
        input << "+ hello + bye ? bye + bye - bye ? bye ? hello";
        run(input, output);
        assert(output.str() == "OK\nOK\nOK\nFAIL\nOK\nFAIL\nOK\n");
    }
    {
        std::stringstream input;
        std::stringstream output;
        input << "+ h0 + h1 + h2 + h3 + h4 + h5 + h6 + h7 + h8 + h9 + h10 ? h3 ? h5 - h4 - h4 ? h4 ? h9";
        run(input, output);
        assert(output.str() == "OK\nOK\nOK\nOK\nOK\nOK\nOK\nOK\nOK\nOK\nOK\nOK\nOK\nOK\nFAIL\nFAIL\nOK\n");
    }
    {
        std::stringstream input;
        std::stringstream output;
        input << "- key ? key - key + key ? key - key ? key + key + key ? key + key - key - key";
        run(input, output);
        assert(output.str() == "FAIL\nFAIL\nFAIL\nOK\nOK\nOK\nFAIL\nOK\nFAIL\nOK\nFAIL\nOK\nFAIL\n");
    }
}

int main(int argc, const char * argv[]) {
    // run(std::cin, std::cout);
    testHashTable();
    return 0;
}
