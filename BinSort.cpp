#include <iostream>

int digit(long long a, int d) {
    return ((a >> d) & 1);
}

template<class T, class Comparator>
int binaryPartition(T *arr, int left, int right, int d, Comparator comp) {
    int pivot = 0;

    int i = left;
    int j = right;

    while (i <= j) {
        for (; comp(digit(arr[i], d), pivot) && i <= j; i++) {}
        for (; !comp(digit(arr[j], d), pivot) && j >= i; j--) {}

        if (i < j) {
            std::swap(arr[i], arr[j]);
        }
    }

    return j;
}

void decToBin(int x, int d) {
    std::cout << "Bin = ";
    for(; d >= 0; d--) {
        std::cout << ((x >> d) & 1);
    }
    std::cout << " Dec = " << x << std::endl;
}

template <class T, class Comparator = std::less_equal<T> >
void binarySort(T *arr, int size, int digit = 63, Comparator comp = Comparator()) {
    if (digit < 0) { return; }
    int part = binaryPartition(arr, 0, size - 1, digit, comp);
    if (part > 0) {
        binarySort(arr, part + 1, digit - 1);
    }
    if (part + 1 < size) {
        binarySort(arr + part + 1, size - (part + 1), digit - 1);
    }
}
 
int main() {
    int n = 0;
    std::cin >> n;
    long long arr[n];
    for (int i = 0; i < n; i++) {
        std::cin >> arr[i];
    }
    binarySort(arr, n);
    for (int i = 0; i < n; i++) {
        std::cout << arr[i] << " ";
    }
    return 0;
}
