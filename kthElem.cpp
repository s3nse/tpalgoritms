#include <iostream>
#include <random>
#include "Dynamic.cpp"

template<class T, class Comparator>
int partition(T *arr, int left, int right, Comparator comp) {

    std::random_device dev;
    std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist6(left, right);

    int pivotIndex = dist6(rng);
    int pivot = arr[pivotIndex];
    
    std::swap(arr[0], arr[pivotIndex]);

    int i = right;
    int j = right;

    while (j > 0) {
        if (!comp(arr[j], pivot)) {
            std::swap(arr[j], arr[i]);
            i--;
            j--;
        } else {
            j--;
        }
    }

    std::swap(arr[i], arr[0]);
    return i;
}

template <class T, class Comparator = std::less<T> >
void kthElement(T *arr, int size, int k, Comparator comp = Comparator()) {
    int left = 0;
    int right = size - 1;

    while (left < right) {
        T part = partition(arr, left, right, comp);
        if (part > k) {
            right = part;
        } else {
            left = part + 1;
        }
    }
}

int main() {
    int k = 0;
    int n = 0;
    std::cin >> n >> k;
    int arr[n];
    for (int i = 0; i < n; ++i) {
        std::cin >> arr[i];
    }
    kthElement(arr, n, k);
    std::cout << arr[k] << std::endl;
    
    return 0;
}