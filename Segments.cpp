#include <iostream>
#include <algorithm>

template<class T>
struct Point {
    T value;
    bool isStart;

    bool operator <(const Point& another) const {
        return value < another.value;
    }

    bool operator >(const Point& another) const {
        return value > another.value;
    }

    bool operator ==(const Point& another) const {
        return value == another.value;
    }

    bool operator >=(const Point& another) const {
        return value >= another.value;
    }

    bool operator <=(const Point& another) const {
        return value <= another.value;
    }

    bool operator !=(const Point& another) const {
        return value != another.value;
    }
};

template<class T, class Comparator>
void Merge(T *first, int fsize, T *sec, int ssize, T *result, Comparator comp) {
    int i = 0;
    int j = 0;

    while (i < fsize && j < ssize) {
        if (comp(first[i], sec[j])) {
            result[i + j] = first[i];
            i++;
        } else {
            result[i + j] = sec[j];
            j++;
        }
    }

    if (i == fsize) {
        std::copy(sec + j, sec + ssize, result + i + j);
    } else {
        std::copy(first + i, first + fsize, result + i + j);
    }
}

template<class T, class Comparator = std::less<T> >
void MergeSort(T *arr, int size, Comparator comp = Comparator()) {
    if (size <= 1) {
        return;
    }

    int firstLen = size / 2;
    int secondLen = size - firstLen;
    MergeSort(arr, firstLen);
    MergeSort(arr + firstLen, secondLen);

    T *buf = new T[size];
    Merge(arr, firstLen, arr + firstLen, secondLen, buf, comp);
    std::copy(buf, buf + size, arr);
}

int segmentsLength(Point<int> *arr, int size) {
    int result = 0;
    int count = 0;
    for (int i = 0; i < size;) {
        int start = i;
        do {
            arr[i].isStart == 0 ? count++ : count--;
            i++;
        } while (count != 0);
        result += (arr[i - 1].value - arr[start].value);
    }
    return result;
}

int main() {
    int n = 0;
    std::cin >> n;
    n *= 2;
    Point<int> arr[n];
    for (int i = 0; i < n; i++) {
        Point<int> buf;
        std::cin >> buf.value;
        buf.isStart = i % 2;
        arr[i] = buf;
    }
    MergeSort(arr, n);
    std::cout << segmentsLength(arr, n) << std::endl;
    return 0;
}
