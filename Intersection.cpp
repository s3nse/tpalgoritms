#include <iostream>
#include "Dynamic.cpp"

int binarySearch(int *arr, int searching, int leftLimit, int rightLimit) {
    int first = leftLimit;
    int last = rightLimit;
    while (first < last) {
        int mid = (first + last) / 2;
        if (arr[mid] < searching) {
            first = mid + 1;
        } else {
            last = mid;
        }
    }
    return (first == rightLimit || arr[first] != searching) ? -1 : first;
}

void limits(int value, int *arr, int size, int leftLimit, int rightLimit) {
    while (value > arr[rightLimit] && rightLimit < size) {
        leftLimit = rightLimit;
        rightLimit *= 2;
        if (rightLimit > size) {
            rightLimit = size;
        }
    }
    std::cout << "left  = " << arr[leftLimit] << "; index = " << leftLimit << std::endl;
    std::cout << "right = " << arr[rightLimit] << "; index = " << rightLimit << std::endl;
}

Dynamic<int> arrayBinarySearch(int *a, int *b, int asize, int bsize) {
    Dynamic<int> result;
    int leftLimit = 0;
    int rightLimit = 1;
    for (int i = 0; i < bsize; ++i) {
        while (b[i] >= a[rightLimit] && rightLimit < asize) {
        leftLimit = rightLimit;
        rightLimit *= 2;
            if (rightLimit > asize) {
                rightLimit = asize;
            }
        }
        if (binarySearch(a, b[i], leftLimit, rightLimit) != -1) {
            leftLimit = binarySearch(a, b[i], leftLimit, rightLimit);
            result.append(a[leftLimit]);
        }
    }
    return result;
}

int main() {
    int n = 0;
    int m = 0;
    std::cin >> n;
    std::cin >> m;
    int exploring[n];
    int desired[m];
    for (int i = 0; i < n; ++i) {
        std::cin >> exploring[i];
    }
    for (int i = 0; i < m; ++i) {
        std::cin >> desired[i];
    }

    Dynamic<int> arr = arrayBinarySearch(exploring, desired, n, m);
    for (int i = 0; i < arr.count(); ++i) {
        std::cout << arr[i] << " ";
    }
    return 0;
}